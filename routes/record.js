const express = require('express');
const recordRoutes = express.Router();
const dbo = require('../db/conn');
const ObjectId = require('mongodb').ObjectId;

recordRoutes.route('/products').get(async function(req, res) {
    try {
        const db_connect = dbo.getDb('zad7_stepik');
        const filteringOptions = ['nazwa', 'cena', 'opis', 'ilosc', 'jednostka_miary'];

        let query = {};
        filteringOptions.forEach((option) => {
            if (req.body[option]) {
                query[option] = req.body[option];
            }
        });


        let sort = {};
        if (req.body.sort) {
            sort[req.body.sort] = (req.body.order === 'desc' ? -1 : 1)
        }
        const result = await db_connect.collection('products').find(query).sort(sort).toArray();
        res.json(result);
    } catch (err) {
        console.error(err);
    }
});

recordRoutes.route('/products').post(async function(req, res) {
    try {
        const insert_values = {
            nazwa: req.body.nazwa,
            cena: req.body.cena,
            opis: req.body.opis,
            ilosc: req.body.ilosc,
            jednostka_miary: req.body.jednostka_miary,
        };

        const product_exists = await dbo.getDb('zad7_stepik').collection('products').findOne({ nazwa: insert_values.nazwa });

        if (product_exists) {
            res.status(409).json({ error: 'Produkt o podanej nazwie już istnieje' });
            return;
        }
        const db_connect = dbo.getDb('zad7_stepik');
        const result = await db_connect.collection('products').insertOne(insert_values);
        res.json(result);
    } catch (err) {
        console.error(err);
    } 
});

recordRoutes.route('/products/:id').put(async function(req, res) {
    try {
        const db_connect = dbo.getDb('zad7_stepik');
        const update_values = {
            nazwa: req.body.nazwa,
            cena: req.body.cena,
            opis: req.body.opis,
            ilosc: req.body.ilosc,
            jednostka_miary: req.body.jednostka_miary,
        };

        const result = await db_connect.collection('products').updateOne({ _id: ObjectId(req.params.id) }, { $set: update_values });
        res.json(result);
    } catch (err) {
        console.error(err);
    }
});

recordRoutes.route('/products/:id').delete(async function(req, res) {
    try {
        const db_connect = dbo.getDb('zad7_stepik');

        const product = await db_connect.collection('products').findOne({ _id: ObjectId(req.params.id) });
        if (!product) {
            res.status(404).json({ error: 'Product not found' });
            return;
        }

        const result = await db_connect.collection('products').deleteOne({ _id: ObjectId(req.params.id) });
        res.json(result);
    } catch (err) {
        console.error(err);
    }
});

recordRoutes.route('/report').get(async function(req, res) {
    try {
        const db_connect = dbo.getDb('zad7_stepik');

        const result = await db_connect.collection('products').aggregate([
             {
                $group: {
                  _id: "$nazwa",
                  totalQuantity: { $sum: "$ilosc" },
                  totalValue: { $sum: { $multiply: ["$cena", "$ilosc"] } }
                }
              },
              {
                $project: {
                  _id: 0, 
                  productName: "$_id",
                  totalQuantity: 1,
                  totalValue: 1
                }
              }
            ]).toArray();

        res.json(result);
    } catch (err) {
        console.error(err);
    }
});

module.exports = recordRoutes;
